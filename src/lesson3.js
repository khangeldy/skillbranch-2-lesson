import bunyan from 'bunyan';
import { AsyncRouter } from 'express-async-router';

const router = AsyncRouter();
const logger = bunyan.createLogger({name: 'api'});

require('es6-promise').polyfill();
require('isomorphic-fetch');

async function getPC() {
	const pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';

	return new Promise(async function(resolve, reject) {
		let pc = {};

		await fetch(pcUrl).then(async (res) => {
			pc = await res.json();
			resolve(pc);
		}).catch((err) => {
			reject(err);
		});

	});

}


router.get('/', function(req, res) {
	return getPC();
});

router.get('/volumes', function(req, res) {
	return getVolumes();
});

router.get(/\S+/, function(req, res) {
	return parameterParse(req.path).then((data) => {
		if(typeof data === 'undefined') {
			res.status(404).send('Not Found');
		}
		res.json(data);
	});
});


router.use(function (err, req, res, next) {
  console.error(err.stack);
	res.status(404).send(err.message);
});


async function parameterParse(stringParams) {

	let pc = await getPC();

	const arrParams = stringParams.replace(/\/$/, '').split('/');
	let result = [];
	logger.info(arrParams);

	result = arrParams.slice(1).reduce((prev, next, i) => {
		if(typeof prev === 'undefined' || next == 'length' && (Array.isArray(prev) || typeof prev === 'string') || !(prev.hasOwnProperty(next))) {
			return undefined;
		} else {
			return prev[next];
		}
	}, pc);

	return result;

}


 async function getVolumes() {

	let pc =  await getPC();
	let result = {};

	pc.hdd.forEach(disc => {
			if(!(result.hasOwnProperty(disc.volume))) {
				result[disc.volume] = disc.size + 'B';
			} else {
				result[disc.volume] = (disc.size + parseInt(result[disc.volume])) + 'B';
			}

	});

	return result;

}

export { router as l3 }; 