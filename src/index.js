import express from 'express';
import cors from 'cors';

import { l2 } from './lesson2';
import { l3 } from './lesson3';
import { toHex } from './hex';

const app = express();

app.use(cors());

// ********
// LESSONS
// ********

// lesson 2

app.use('/l2', l2);
app.use('/task3a', l3);
app.use('/colortohex', toHex);


app.get('/', function(req, res) {
	res.send('GO PLIZE TO LESSON');
});



const server = app.listen(3000, function() {
	var host = server.address().address;
	var port = server.address().port;
	console.log('Example app listening at http://%s:%s', host, port);
});