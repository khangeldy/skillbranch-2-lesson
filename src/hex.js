import bunyan from 'bunyan';
import { AsyncRouter } from 'express-async-router';

const router = AsyncRouter();
const logger = bunyan.createLogger({name: 'hex'});

// reqular expressions
const hexReg = /(^#?[0-f]{3}$)|(^#?[0-f]{6}$)/;
const rgbReg = /^rgb\(\s*(\d+),\s*(\d+)\s*,\s*(\d+)\s*\)$/;
const hslReg = /^hsl\((\d+),\s*(\d+%),\s*(\d+%)\)$/;

// Middleware return string if  color query undefined 
router.use('/', function (req, res, next) {
	if(!req.query.color) {
		res.send('Invalid color');
	} else {
		next();
	}
});

router.get('/', function(req, res) {
	const color = req.query.color.trim().replace(/%20/g, "").toLowerCase(); // remove white spaces and lowercase string
	logger.info('query', color);
	return toHex(color);
});

// Main Logic
// q = string 
// return string type
function toHex(q) {

	// if q is hex format
	if(hexReg.test(q)) {

		// for remove # char 
		if(/%23/.test(q) || /#/.test(q)) {
			q = q.replace(/(%23)|(#)/, "");
		}

		// if length of hex is 3
		if(q.length === 3) {
			return '#' + q.split('').map(x => x.toString() + x).join('');
		}

		return '#' + q;

	} else if(rgbReg.test(q) && rangeValid(q)) { // if format is rgb

		return rgb2hex(q);

	} else if(hslReg.test(q)) { // if format is hsl

		// convert hsl to rgb
		const hsl = hslToRgb(q);
		logger.info('hsl come in to rgb:', hsl);

		return rgb2hex(hsl);

	} else { // if of this formats not match
		return 'Invalid color';
	}
}

// convert rgb 2 hex
function rgb2hex(color) {

		const rgb = color.trim().match(rgbReg); // split to array

		logger.info('after rgb parse:', rgb);
		logger.info('after color parse:', color);

		function hex(x) {
				return ("0" + parseInt(x).toString(16)).slice(-2);
		}

		return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}

// verify if r, b, g is smaller than 255
function rangeValid(string) {

	const arr = string.match(/\d+/g);
	
	return arr.every((number) => {
		if(number >= 0 && number <= 255) {
			return number;
		}
	})
}


/**
 * Converts an HSL color value to RGB. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
 * Assumes h, s, and l are contained in the set [0, 1] and
 * returns r, g, and b in the set [0, 255].
 *
 * @param   {number}  h       The hue
 * @param   {number}  s       The saturation
 * @param   {number}  l       The lightness
 * @return  {Array}           The RGB representation
 */
function hslToRgb(string){
		var r, g, b;

		var [ h, s, l ] = string.match(/\d+/g).map((s,index) => {
			if(index === 0) {
				return Math.round(((+s)/ 360) * 10) / 10;
			}
			return (+s) / 100
		});

		logger.info('hsl come in ', h,s,l);

		if(h > 345 && s > 100 && l > 100 && [ h, s, l].some(x => x < 0)) {
			return false;
		}

		if (s == 0) {
			r = g = b = l; // achromatic
		} else {
			function hue2rgb(p, q, t) {
				if (t < 0) t += 1;
				if (t > 1) t -= 1;
				if (t < 1/6) return p + (q - p) * 6 * t;
				if (t < 1/2) return q;
				if (t < 2/3) return p + (q - p) * (2/3 - t) * 6;
				return p;
			}

			var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
			var p = 2 * l - q;

			r = hue2rgb(p, q, h + 1/3);
			g = hue2rgb(p, q, h);
			b = hue2rgb(p, q, h - 1/3);
		}

		return ` rgb(${Math.round(r) * 255}, ${Math.round(g) * 255}, ${Math.round(b) * 255})`;
		// return [ r * 255, g * 255, b * 255 ];
}

export { router as  toHex }