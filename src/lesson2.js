import express from 'express';

const router  = express.Router();

router.get('/', function(req, res) {
	res.redirect('/l2/sum');
});

router.get('/sum', (req, res) => {
	res.send(sum(req.query.a, req.query.b));
});

router.get('/name',  (req, res) => {
	res.send(name(req.query.fullname));
});

router.get('/username', (req, res) => {
	res.send(getUserName(req.query.username));
});


// exicise 1
function sum(a = 0, b = 0) {
	const sum  = (+a) + (+b);

	return (isNaN(sum) ? 0 : sum).toString();
}

// exicise 2
function name(fullname) {
	console.log('entry', fullname);
	const err = 'Invalid fullname';
	let fullNameArray = [];

	if(fullname && fullname.length > 0 && !/[\d_\/]/.test(fullname)) {
			fullNameArray = fullname.trim().toLowerCase().split(/\s+/);
	} else {
		return err;
	}
	try {
		console.log('output',fullNameArray.length);
		console.dir(fullNameArray);
	} catch(err) {

	}


	switch (fullNameArray.length) {
		case 3:
			return Capitalize(fullNameArray[2]) + firstLetter(fullNameArray[0]) + firstLetter(fullNameArray[1]);
		case 2:
			return Capitalize(fullNameArray[1]) + firstLetter(fullNameArray[0]);
		case 1:
			return Capitalize(fullNameArray[0]);
		default:
			return err;
	}

	function firstLetter(string) {
		return  ' ' + string.charAt(0).toUpperCase() + '.';
	}

	function Capitalize(string) {
		return  string.charAt(0).toUpperCase() + string.slice(1);
	}

	return fullNameArray

}


// exicise 3
function getUserName(query) {
	const pattern = /[a-z-._]+/ig;
	let result = 'Invalid username';

	if(!query) {
		return result;
	}

	let newQuery = query.replace(/.*(=?\/\/)/i, '');

	if(newQuery.match(pattern).length >= 2) {
		newQuery = newQuery.replace(/[a-z0-9-.]*\//i, '');

		if(/[^a-zA-Z0-9_.-@]/i.test(newQuery.match(/[^\/?]+/)[0])) {
			return result;
		}
	}
	if(/^@/.test(newQuery.match(pattern)[0])) {
		return newQuery.match(pattern)[0];
	}
	result = '@' + newQuery.match(pattern)[0];

	return result;


}

export { router as l2 };